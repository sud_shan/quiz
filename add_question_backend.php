<?php
require_once 'include/session.php';
require_once 'include/database.php';
if(!isset($_POST))
	header("Location:logout.php");
$ques_name=$_POST['ques_name'];
$option_1=$_POST['option_1'];
$option_2=$_POST['option_2'];
$option_3=$_POST['option_3'];
$option_4=$_POST['option_4'];
$correct_option=$_POST['correct_option'];
$sec_id=$_POST['sec_id'];
$quiz_creator=$_SESSION['user'];
$sql="insert into questions(question_text,option_1,option_2,option_3,option_4,correct_option,section_id) values(:question_text,:option_1,:option_2,:option_3,:option_4,:correct_option,:section_id);";
$stmt=$dbh->prepare($sql);
$stmt->bindParam(":question_text",$ques_name);
$stmt->bindParam(":option_1",$option_1);
$stmt->bindParam(":option_2",$option_2);
$stmt->bindParam(":option_3",$option_3);
$stmt->bindParam(":option_4",$option_4);
$stmt->bindParam(":correct_option",$correct_option);
$stmt->bindParam(":section_id",$sec_id);
if($stmt->execute())
	echo "Done";
else
	echo "Not done";
?>