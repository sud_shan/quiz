<?php
require_once 'include/session.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

  <title>Maintain Quiz</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/dashboard.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>

    <body>

      <?php require_once 'admin_top.php';
            require_once 'include/database.php';
            $creator=$_SESSION['user'];
            $sql="select * from quiz where quiz_creator=:creator;";
$stmt=$dbh->prepare($sql);
$stmt->bindParam(":creator",$creator);

$stmt->execute();

      ?>
      <div class="container-fluid">
        <div class="row">
          <?php require_once 'admin_side.php';?>
          <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <form method="post" action="maintain_quiz_submit.php">
            <table class="table table-hover">
            <tr>
            <th>#</th><th>Quiz Name</th><th>Creator</th><th>Created at</th><th>Select</th>
            </tr>
            <?php
            $i=1;
            while($r=$stmt->fetch())
            {
              ?>
            <tr>
            <td><?php echo $i;?></td><td><?php echo $r['quiz_name'];?></td><td><?php echo $r['quiz_creator'];?></td><td><?php echo $r['quiz_creation_time'];?></td><td><input type="radio" name="radio1" value="<?php echo $r['id'];?>"></td>
            </tr>
            <?php
            $i++;
            }?>
            </table>
            <button type="submit" class="btn btn-primary" value="maintain" id="maintain" name="submit" >Maintain Sections</button>
            <button type="submit" name="submit" class="btn btn-danger" id="delete"  value="delete">Delete Quiz</button>
          </form>
          </div>
        </div>
      </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>

  </body>
  </html>
