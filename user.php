<?php
require_once 'include/session_user.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

  <title>Quiz</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/dashboard.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>

    <body>

      <?php require_once 'user_top.php';
      require_once 'include/database.php';
      $creator=$_SESSION['user'];
      $sql="select * from quiz ";
      $stmt=$dbh->prepare($sql);
      $stmt->bindParam(":creator",$creator);

      $stmt->execute();

      ?>
      <div class="container-fluid">
        <div class="row">

          <?php require_once 'user_side.php';?>
          <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <form method="post" action="attempt_quiz.php">
              <table class="table table-hover">
                <tr>
                  <th>#</th><th>Quiz Name</th><th>Creator</th><th>Created at</th><th>Select</th>
                </tr>
                <?php
                $i=1;
                while($r=$stmt->fetch())
                {
                  ?>
                  <tr>
                    <td><?php echo $i;?></td><td><?php echo $r['quiz_name'];?></td><td><?php echo $r['quiz_creator'];?></td><td><?php echo $r['quiz_creation_time'];?></td><td><input type="radio" name="radio1" value="<?php echo $r['id'];?>"></td>
                  </tr>
                  <?php
                  $i++;
                }?>
              </table>
              <button type="submit" class="btn btn-primary" value="maintain" id="maintain" name="submit" >Attempt Quiz</button>
            </form>
            <form id="quiz1" action="quiz_submit.php" method="post">
              <div class="panel-group" id="accordion">

                


                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#1">
                        1                        </a>
                      </h4>
                    </div>
                    <div id="1" class="panel-collapse collapse">
                      <div class="panel-body">
                        
                        <ul class="list-group" >
                          <li class="list-group-item">Question2<br></li>
                          <li class="list-group-item"><input value="1_1_2_1" class="rad_1"type="radio" name="1">1<br></li>
                          <li class="list-group-item"><input value="1_1_2_2" type="radio" class="rad_1"name="1">2<br></li>
                          <li class="list-group-item"><input value="1_1_2_3" type="radio" class="rad_1"name="1">3<br></li>
                          <li class="list-group-item"><input value="1_1_2_4" type="radio" class="rad_1"name="1">4<br></li>
                          <button type="button" id="1" onClick="reset1(this.id)">Reset</button>
                        </ul>   

                        
                        <ul class="list-group" >
                          <li class="list-group-item">Question3<br></li>
                          <li class="list-group-item"><input value="1_1_3_1" class="rad_2"type="radio" name="2">1<br></li>
                          <li class="list-group-item"><input value="1_1_3_2" type="radio" class="rad_2"name="2">2<br></li>
                          <li class="list-group-item"><input value="1_1_3_3" type="radio" class="rad_2"name="2">3<br></li>
                          <li class="list-group-item"><input value="1_1_3_4" type="radio" class="rad_2"name="2">4<br></li>
                          <button type="button" id="2" onClick="reset1(this.id)">Reset</button>
                        </ul>   

                        
                        <ul class="list-group" >
                          <li class="list-group-item">Question5<br></li>
                          <li class="list-group-item"><input value="1_1_5_1" class="rad_3"type="radio" name="3">1<br></li>
                          <li class="list-group-item"><input value="1_1_5_2" type="radio" class="rad_3"name="3">2<br></li>
                          <li class="list-group-item"><input value="1_1_5_3" type="radio" class="rad_3"name="3">3<br></li>
                          <li class="list-group-item"><input value="1_1_5_4" type="radio" class="rad_3"name="3">4<br></li>
                          <button type="button" id="3" onClick="reset1(this.id)">Reset</button>
                        </ul>   

                        
                        <ul class="list-group" >
                          <li class="list-group-item">Question7<br></li>
                          <li class="list-group-item"><input value="1_1_7_1" class="rad_4"type="radio" name="4">1<br></li>
                          <li class="list-group-item"><input value="1_1_7_2" type="radio" class="rad_4"name="4">2<br></li>
                          <li class="list-group-item"><input value="1_1_7_3" type="radio" class="rad_4"name="4">3<br></li>
                          <li class="list-group-item"><input value="1_1_7_4" type="radio" class="rad_4"name="4">4<br></li>
                          <button type="button" id="4" onClick="reset1(this.id)">Reset</button>
                        </ul>   

                        
                        <ul class="list-group" >
                          <li class="list-group-item">Question9<br></li>
                          <li class="list-group-item"><input value="1_1_9_1" class="rad_5"type="radio" name="5">1<br></li>
                          <li class="list-group-item"><input value="1_1_9_2" type="radio" class="rad_5"name="5">2<br></li>
                          <li class="list-group-item"><input value="1_1_9_3" type="radio" class="rad_5"name="5">3<br></li>
                          <li class="list-group-item"><input value="1_1_9_4" type="radio" class="rad_5"name="5">4<br></li>
                          <button type="button" id="5" onClick="reset1(this.id)">Reset</button>
                        </ul>   

                      </div>
                    </div>
                  </div>
                  


                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#2">
                          2                        </a>
                        </h4>
                      </div>
                      <div id="2" class="panel-collapse collapse">
                        <div class="panel-body">
                          
                          <ul class="list-group" >
                            <li class="list-group-item">Question 1          <br></li>
                            <li class="list-group-item"><input value="1_2_11_1" class="rad_6"type="radio" name="6">12<br></li>
                            <li class="list-group-item"><input value="1_2_11_2" type="radio" class="rad_6"name="6">12<br></li>
                            <li class="list-group-item"><input value="1_2_11_3" type="radio" class="rad_6"name="6">3<br></li>
                            <li class="list-group-item"><input value="1_2_11_4" type="radio" class="rad_6"name="6">45<br></li>
                            <button type="button" id="6" onClick="reset1(this.id)">Reset</button>
                          </ul>   

                          
                          <ul class="list-group" >
                            <li class="list-group-item">Question 4 <br></li>
                            <li class="list-group-item"><input value="1_2_14_1" class="rad_7"type="radio" name="7">12<br></li>
                            <li class="list-group-item"><input value="1_2_14_2" type="radio" class="rad_7"name="7">12<br></li>
                            <li class="list-group-item"><input value="1_2_14_3" type="radio" class="rad_7"name="7">3<br></li>
                            <li class="list-group-item"><input value="1_2_14_4" type="radio" class="rad_7"name="7">45<br></li>
                            <button type="button" id="7" onClick="reset1(this.id)">Reset</button>
                          </ul>   

                          
                          <ul class="list-group" >
                            <li class="list-group-item">Question 6 <br></li>
                            <li class="list-group-item"><input value="1_2_16_1" class="rad_8"type="radio" name="8">12<br></li>
                            <li class="list-group-item"><input value="1_2_16_2" type="radio" class="rad_8"name="8">12<br></li>
                            <li class="list-group-item"><input value="1_2_16_3" type="radio" class="rad_8"name="8">3<br></li>
                            <li class="list-group-item"><input value="1_2_16_4" type="radio" class="rad_8"name="8">45<br></li>
                            <button type="button" id="8" onClick="reset1(this.id)">Reset</button>
                          </ul>   

                          
                          <ul class="list-group" >
                            <li class="list-group-item">Question 7 <br></li>
                            <li class="list-group-item"><input value="1_2_17_1" class="rad_9"type="radio" name="9">12<br></li>
                            <li class="list-group-item"><input value="1_2_17_2" type="radio" class="rad_9"name="9">12<br></li>
                            <li class="list-group-item"><input value="1_2_17_3" type="radio" class="rad_9"name="9">3<br></li>
                            <li class="list-group-item"><input value="1_2_17_4" type="radio" class="rad_9"name="9">45<br></li>
                            <button type="button" id="9" onClick="reset1(this.id)">Reset</button>
                          </ul>   

                          
                          <ul class="list-group" >
                            <li class="list-group-item">Question 8<br></li>
                            <li class="list-group-item"><input value="1_2_18_1" class="rad_10"type="radio" name="10">12<br></li>
                            <li class="list-group-item"><input value="1_2_18_2" type="radio" class="rad_10"name="10">12<br></li>
                            <li class="list-group-item"><input value="1_2_18_3" type="radio" class="rad_10"name="10">3<br></li>
                            <li class="list-group-item"><input value="1_2_18_4" type="radio" class="rad_10"name="10">45<br></li>
                            <button type="button" id="10" onClick="reset1(this.id)">Reset</button>
                          </ul>   

                        </div>
                      </div>
                    </div>
                    
                  </div>

                  <button  type="submit">Submit</button>

                </form>


              </div>
            </div>
          </div>


        </div>
      </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>

  </body>
  </html>
