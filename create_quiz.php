<?php
require_once 'include/session.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

  <title>Create Quiz</title>

  
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/dashboard.css" rel="stylesheet">
 <style>
  .form-control{
    width: 400px;
  }
  </style>
  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>

    <body>

      <?php require_once 'admin_top.php';?>
      <div class="container-fluid">
        <div class="row">
          <?php require_once 'admin_side.php';?>
          <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <form role="form" method="post" action="create_quiz_submit.php">
  <div class="form-group">
    <label for="quiz_name">Quiz Name</label>
    <input type="text" class="form-control" id="quiz_name" name="quiz_name" placeholder="Enter a name for the Quiz" required>
  </div>
  <div class="form-group">
    <label for="quiz_desc">Quiz Description</label>
    <textarea class="form-control" rows="3" id="quiz_desc" name="quiz_desc" placeholder="Enter a description of the quiz" required></textarea>
  </div>
  
  
  <button type="submit" class="btn btn-default">Submit</button>
</form>
          </div>
        </div>
      </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
  </body>
  </html>
