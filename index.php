
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">

    <title>Assessment Portal</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/justified-nav.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">

      <div class="masthead">
        <h3 class="text-muted">Assessment</h3>
        <ul class="nav nav-justified">
          <li class="active"><a href="#">Home</a></li>
          <li><a href="#">About</a></li>
          <li><a href="#">Contact</a></li>
          <li><a href="#signup" data-toggle="modal">Signup</a></li>
          <li><a href="#login" data-toggle="modal">Login</a></li>
        </ul>
      </div>

      <!-- Jumbotron -->
      <div class="jumbotron">
        <h1>Marketing stuff!</h1>
        <p class="lead">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet.</p>
        <p><a class="btn btn-lg btn-success" href="#" role="button">Get started today</a></p>
      </div>

      <!-- Example row of columns -->
      <div class="row">
        <div class="col-lg-4">
          <h2>Safari bug warning!</h2>
          <p class="text-danger">As of v7.0.1, Safari exhibits a bug in which resizing your browser horizontally causes rendering errors in the justified nav that are cleared upon refreshing.</p>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-primary" href="#" role="button">View details &raquo;</a></p>
        </div>
        <div class="col-lg-4">
          <h2>Heading</h2>
          <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
          <p><a class="btn btn-primary" href="#" role="button">View details &raquo;</a></p>
       </div>
        <div class="col-lg-4">
          <h2>Heading</h2>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa.</p>
          <p><a class="btn btn-primary" href="#" role="button">View details &raquo;</a></p>
        </div>
      </div>
      <div class="modal fade" id="signup" style="background:rgba(102,0,204,0.1);" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">


      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header" style="background:rgba(0,51,102,1);">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" style="color:white;" id="myModalLabel">Sign Up</h4>
          </div>
          <div class="modal-body" style="background:rgba(0,51,102,.0);">
           <?php
           if(isset($_GET['message']))
           {
            ?>
            <div class="alert alert-success alert-block">
             <a class="close" data-dismiss="alert" href="#">&times;</a>
             <h4 class="alert-heading"></h4>

             <?php
             echo htmlentities($_GET['message']);
             ?>
           </div>
           <?php
           
         }

         ?>



         <form role="form" method="post" action="register_user.php" enctype="multipart/form-data">
          <div class="form-group">
           <label for="exampleInputEmail1">Full Name</label>
           <input type="text" class="form-control" id="exampleInputEmail1" name="register_name" placeholder="Enter Full Name" required>
         </div>

         <div class="form-group">
           <label for="exampleInputEmail1">Email address</label>
           <input type="email" class="form-control" id="exampleInputEmail1" name="register_email" placeholder="Enter email" required>
         </div>
         <div class="form-group">
           <label for="exampleInputPassword1">Password</label>
           <input type="password" class="form-control" id="exampleInputPassword1" name="register_password" placeholder="Password" required>
         </div>
         <div class="form-group">
           <label for="exampleInputPassword1">Re-Enter Password</label>
           <input type="password" class="form-control" id="exampleInputPassword1" name="register_re_password" placeholder="Password" required>
         </div>
         
         <div class="checkbox">
           <label>
             <input type="radio" name ="user_type" value="s" required>
             Signup as Student
           </label>
           <br>
           <label>
             <input type="radio" name ="user_type" value="t" required>
             Signup as Teacher
           </label>

         </div>


         <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">


  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"  style="background:rgba(0,51,102,1);">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" style="color:white;" id="myModalLabel">Login</h4>
      </div>
      <div class="modal-body">
       <?php 
       if(isset($_GET['message']))
       {
         ?>
         <div class="alert alert-warning alert-block">
           <a class="close" data-dismiss="alert" href="#">&times;</a>
           <h4 class="alert-heading"></h4>
           <?php echo htmlentities($_GET['message']);?>

         </div>
         <?php
       }
       ?>

       <form role="form" method="post" action="login_user.php" enctype="multipart/form-data">


        <div class="form-group">
         <label for="exampleInputEmail1">Email address</label>
         <input type="email" class="form-control" id="exampleInputEmail1" name="register_email" placeholder="Enter email" required>
       </div>
       <div class="form-group">
         <label for="exampleInputPassword1">Password</label>
         <input type="password" class="form-control" id="exampleInputPassword1" name="register_password" placeholder="Password" required>
       </div>


       <div class="checkbox">
         <label>
           <input type="radio" name ="user_type" value="s" required>
           Login as Student
         </label>
         <br>
         <label>
           <input type="radio" name ="user_type" value="t" required>
           Login as Teacher
         </label>

       </div>


       <div class="modal-footer">
       <button onclick="forgot_password()" type="button" class="btn btn-primary" >Forgot Password</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
  </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="forgot" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">


  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"  style="background:rgba(0,51,102,1);">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" style="color:white;" id="myModalLabel">Forgot Password</h4>
      </div>
      <div class="modal-body">
        
       

       <form role="form" method="post" action="reset_password.php" enctype="multipart/form-data">


        <div class="form-group">
         <label for="exampleInputEmail1">Email address</label>
         <input type="email" class="form-control" id="exampleInputEmail1" name="register_email" placeholder="Enter email" required>
       </div>
       


       


       <div class="modal-footer">
       
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

        <button type="submit" class="btn btn-primary">Reset Password</button>
      </form>
    </div>
  </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

      <!-- Site footer -->
      <div class="footer">
        <p>&copy; Company 2014</p>
      </div>

    </div> <!-- /container -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/holder.js"></script>
    <script type="text/javascript">
  function forgot_password()
  {

    $('#login').modal('hide');
    $('#forgot').modal('show');


  }
</script>
<?php
    if(isset($_GET['message']))
    {
      ?>
      <script>
        $('#login').modal('show');
      </script>
      <?php
    }
    ?>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  </body>
</html>
