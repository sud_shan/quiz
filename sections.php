<?php
require_once 'include/session.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

  <title>Sections</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/dashboard.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
      <style>
        .form-control{
          width: 400px;
        }
      </style>
    </head>

    <body id="body1">

      <?php require_once 'admin_top.php';
      require_once 'include/database.php';
      $sql1="select * from quiz where id=:quiz_id;";
      $stmt1=$dbh->prepare($sql1);
      $stmt1->bindParam(":quiz_id",$q);
      $q=$_SESSION['action'];
      $stmt1->execute();
      $r1=$stmt1->fetch();
      $stmt1->execute();
      $creator=$_SESSION['user'];
      $sql="select * from sections where quiz_id=:quiz_id;";
      $stmt=$dbh->prepare($sql);
      $stmt->bindParam(":quiz_id",$q);

      $stmt->execute();?>
      <div class="container-fluid">
        <div class="row">
          <?php require_once 'admin_side.php';?>

          <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <form id="quiz_update" class="form-inline" role="form">
              <div class="form-group">
                <label class="sr-only" for="quiz_name">Quiz Name</label>
                <input type="text" class="form-control" id="quiz_name" name="quiz_name" value="<?php echo $r1['quiz_name'];?>">
              </div>
              <input type="hidden" name="quiz_id" value="<?php echo $q;?>">
              <div class="form-group">
                <label class="sr-only" for="quiz_desc">Quiz_desc</label>
                <input type="text" class="form-control" id="quiz_desc" name="quiz_desc" value="<?php echo $r1['quiz_desc'];?>">
              </div>

              <button type="submit" class="btn btn-default">Update</button>
            </form>
            <br><br>
            <form method="post" action="section_submit.php" onsubmit="return confirm('Do you really want to continue?')";>
              <table id="sections" class="table table-hover">
                <tr>
                  <th>#</th><th>Section Name</th><th>Section Description</th><th>Per Ques Mark</th><th>Negative Mark</th><th>Created at</th><th>Select</th>
                </tr>
                <?php
                $i=1;
                while($r=$stmt->fetch())
                {
                  ?>
                  <tr>
                    <td><?php echo $i;?></td><td><?php echo $r['section_name'];?></td><td><?php echo $r['section_desc'];?></td><td><?php echo $r['sec_mark'];?></td><td><?php echo $r['neg_mark'];?></td><td><?php echo $r['section_creation_time'];?></td><td><input type="radio" name="radio1" value="<?php echo $r['id'];?>"></td>
                  </tr>
                  <?php
                  $i++;
                }?>
              </table>
              <button type="submit" class="btn btn-primary" value="maintain" id="maintain" name="submit" >Maintain Section</button>
              <button type="submit" name="submit" class="btn btn-danger" id="delete"  value="delete">Delete Section</button>
            </form>
            <br><br>
            <div class="col-md-5">
              <form id="add_section" role="form"  >
                <div class="form-group">
                  <label for="section_name">Section Name</label>
                  <input type="text" class="form-control" id="section_name" name="section_name" placeholder="Enter a name for the Section" required>
                </div>
                <input type="hidden" name="quiz_id" value="<?php echo $q;?>">
                <div class="form-group">
                  <label for="section_desc">Section Description</label>
                  <textarea class="form-control" rows="3" id="section_desc" name="section_desc" placeholder="Enter a description of the section" required></textarea>
                </div>
                <div class="form-group">
                <label  for="sec_mark">Per Question Mark</label>
                <input type="text" class="form-control" id="sec_mark" name="sec_mark" >
              </div>
              <div class="form-group">
                <label  for="neg_mark">Negative Mark</label>
                <input type="text" class="form-control" id="neg_mark" name="neg_mark">
              </div>


                <button type="submit" class="btn btn-default">Submit</button>
              </form>
            </div>




          </div>
        </div>
      </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <script>
    $(function () {
    $('#quiz_update').on('submit', function (e) {
            $.ajax({
              type: 'post',
              url: 'quiz_update_backend.php',
              data: $('#quiz_update').serialize(),
              success: function (msg) {
               $('#quiz_update').load(window.location.href + ' #quiz_update');
              },
              error: function(){
                
              }
              

            });
            e.preventDefault();
          });
      $('#add_section').on('submit', function (e) {
            $.ajax({
              type: 'post',
              url: 'add_section_backend.php',
              data: $('#add_section').serialize(),
              success: function (msg) {
               $('#sections').load(window.location.href + ' #sections');
              },
              error: function(){
                
              }
              

            });
            e.preventDefault();
          });
        });
      </script>


  </body>
  </html>
