<?php
require_once 'include/session.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

  <title>Sections</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/dashboard.css" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy this line! -->
  <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
      <style>
        .form-control{
          width: 400px;
        }
      </style>
    </head>

    <body id="body1">

      <?php require_once 'admin_top.php';
      require_once 'include/database.php';
      $sql1="select * from sections where id=:sec_id;";
      $stmt1=$dbh->prepare($sql1);
      $stmt1->bindParam(":sec_id",$q);
      $q=$_SESSION['action1'];
      $stmt1->execute();
      $r1=$stmt1->fetch();
      $stmt1->execute();
      $creator=$_SESSION['user'];
      $sql="select * from questions where section_id=:sec_id;";
      $stmt=$dbh->prepare($sql);
      $stmt->bindParam(":sec_id",$q);

      $stmt->execute();?>
      <div class="container-fluid">
        <div class="row">
          <?php require_once 'admin_side.php';?>

          <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <form id="quiz_update" class="form-inline" role="form">
              <div class="form-group">
                <label class="sr-only" for="sec_name">Section Name</label>
                <input type="text" class="form-control" id="sec_name" name="sec_name" value="<?php echo $r1['section_name'];?>">
              </div>
              <input type="hidden" name="sec_id" value="<?php echo $q;?>">
              <div class="form-group">
                <label class="sr-only" for="quiz_desc">Section_desc</label>
                <input type="text" class="form-control" id="section_desc" name="section_desc" value="<?php echo $r1['section_desc'];?>">
              </div>
              <div class="form-group">
                <label class="sr-only" for="sec_mark">Per Question Mark</label>
                <input type="text" class="form-control" id="sec_mark" name="sec_mark" value="<?php echo $r1['sec_mark'];?>">
              </div>
              <div class="form-group">
                <label class="sr-only" for="neg_mark">Negative Mark</label>
                <input type="text" class="form-control" id="neg_mark" name="neg_mark" value="<?php echo $r1['neg_mark'];?>">
              </div>

              <button type="submit" class="btn btn-default">Update</button>
            </form>
            <br><br>
            <form method="post" action="delete_questions.php" onsubmit="return confirm('Do you really want to continue?')";>
              <table id="questions" class="table table-hover">
                <tr>
                  <th>#</th><th>Question</th><th>Option1</th><th>Option 2</th><th>Option 3</th><th>Option 4</th><th>Correct Option</th><th>Select</th>
                </tr>
                <?php
                $i=1;
                while($r=$stmt->fetch())
                {
                  ?>
                  <tr>
                    <td><?php echo $i;?></td><td><?php echo $r['question_text'];?></td><td><?php echo $r['option_1'];?></td><td><?php echo $r['option_2'];?></td><td><?php echo $r['option_3'];?></td><td><?php echo $r['option_4'];?></td><td><?php echo $r['correct_option'];?></td><td><input type="radio" name="radio1" value="<?php echo $r['id'];?>"></td>
                  </tr>
                  <?php
                  $i++;
                }?>
              </table><!-- 
              <button type="submit" class="btn btn-primary" value="maintain" id="maintain" name="submit" ></button> -->
              <button type="submit" name="submit" class="btn btn-danger" id="delete"  value="delete">Delete</button>
            </form>
            <br><br>
            <div class="col-md-5">
              <form id="add_question" role="form"  >
                <div class="form-group">
                  <label for="ques_name">Question</label>
                  <textarea class="form-control" id="ques_name" name="ques_name"  required>
                  </textarea>
                </div>
                <input type="hidden" name="sec_id" value="<?php echo $q;?>">
                <div class="form-group">
                  <label for="option_1">Option 1</label>
                  <input type="text" class="form-control" id="option_1" name="option_1" placeholder="Option 1" required>
                </div>
                <div class="form-group">
                  <label for="option_2">Option 2</label>
                  <input type="text" class="form-control" id="option_2" name="option_2" placeholder="Option 2" required>
                </div>
                <div class="form-group">
                  <label for="option_3">Option 3</label>
                  <input type="text" class="form-control" id="option_3" name="option_3" placeholder="Option 3" required>
                </div>
                <div class="form-group">
                  <label for="option_4">Option 4</label>
                  <input type="text" class="form-control" id="option_4" name="option_4" placeholder="Option 4" required>
                </div>
                <div class="form-group">
                  <label for="correct_option">Correct Option</label>
                  <input type="text" class="form-control" id="correct_option" name="correct_option" placeholder="correct_option" required>
                </div>

                <button type="submit" class="btn btn-default">Submit</button>
              </form>
            </div>




          </div>
        </div>
      </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <script>
    $(function () {
    // $('#quiz_update').on('submit', function (e) {
    //         $.ajax({
    //           type: 'post',
    //           url: 'quiz_update_backend.php',
    //           data: $('#quiz_update').serialize(),
    //           success: function (msg) {
    //            $('#quiz_update').load(window.location.href + ' #quiz_update');
    //           },
    //           error: function(){
                
    //           }
              

    //         });
    //         e.preventDefault();
    //       });
      $('#add_question').on('submit', function (e) {
            $.ajax({
              type: 'post',
              url: 'add_question_backend.php',
              data: $('#add_question').serialize(),
              success: function (msg) {
                
               $('#questions').load(window.location.href + ' #questions');
              },
              error: function(){
                alert("error");
              }
              

            });
            e.preventDefault();
          });
        });
      </script>


  </body>
  </html>
