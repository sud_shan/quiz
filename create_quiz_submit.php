<?php
require_once 'include/session.php';
require_once 'include/database.php';
if(!isset($_POST))
	header("Location:logout.php");
$quiz_name=$_POST['quiz_name'];
$quiz_desc=$_POST['quiz_desc'];
$quiz_creator=$_SESSION['user'];
$sql="insert into quiz(quiz_name,quiz_desc,quiz_creator) values(:quiz_name,:quiz_desc,:quiz_creator);";
$stmt=$dbh->prepare($sql);
$stmt->bindParam(":quiz_name",$quiz_name);
$stmt->bindParam(":quiz_desc",$quiz_desc);
$stmt->bindParam(":quiz_creator",$quiz_creator);
$stmt->execute();
?>