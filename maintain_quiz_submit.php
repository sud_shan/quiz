<?php
require_once 'include/session.php';
require_once 'include/database.php';
if(!isset($_POST))
	header("Location:logout.php");
$action=$_POST['submit'];
$quiz_id=$_POST['radio1'];
if($action=="delete")
{
	$sql="delete from quiz where id=:id and quiz_creator=:creator;";
	$stmt=$dbh->prepare($sql);
	$stmt->bindParam(":id",$quiz_id);
	$stmt->bindParam(":creator",$creator);
	$creator=$_SESSION['user'];		
	if($stmt->execute())
		header("Location:maintain_quiz.php?id=Successfully Deleted");
}
else
if($action=="maintain")
{
	$_SESSION['action']=$quiz_id;
	header("Location:sections.php");
}		
	?>