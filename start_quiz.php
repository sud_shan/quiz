<?php
require_once 'include/session_user.php';
require_once 'include/database.php';
$user=$_SESSION['user'];
$quiz_id=$_SESSION['quiz'];
$minutes = 100; // Enter minutes
$seconds = 0; // Enter seconds
$time_limit = ($minutes * 60) + $seconds + 1; // Convert total time into seconds
if(!isset($_SESSION["start_time"])){$_SESSION["start_time"] = mktime(date(G),date(i),date(s),date(m),date(d),date(Y)) + $time_limit;} // Add $time_limit (total time) to start time. And store into session variable.

$sql="select * from submissions where register_email=:user and quiz_id=:quiz_id";

$stmt=$dbh->prepare($sql);
$stmt->bindParam(":user",$user);
$stmt->bindParam(":quiz_id",$quiz_id);
$stmt->execute();
$r=$stmt->fetch();
if($r['status']=="yes")
  header("Location:user.php?id=Already Attempted");
else
{
  $sql="select distinct(sec_id) from submissions where register_email=:user and quiz_id=:quiz_id";

  $stmt=$dbh->prepare($sql);
  $stmt->bindParam(":user",$user);
  $stmt->bindParam(":quiz_id",$quiz_id);
  $stmt->execute();


  ?>
  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">

    <title>Quiz</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
    <style>
      #txt {
        border:2px solid red;
        font-family:verdana;
        font-size:16pt;
        font-weight:bold;
        background: #FECFC7;
        width:70px;
        text-align:center;
        color:#000000;
      }
    </style>

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    </head>

    <body>

      <?php require_once 'user_top.php';
      require_once 'include/database.php';



      ?><input id="txt" readonly>
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-offset-3">
          <form id="quiz1" action="quiz_submit.php" method="post">
              <div class="panel-group" id="accordion">

                <?php
                $i=1;
                while($r=$stmt->fetch())
                {

                  $sec=$r['sec_id'];
                  //echo $sec;
                  $sql1="select * from submissions where register_email=:user and quiz_id=:quiz_id and sec_id='$sec'";

                  $stmt1=$dbh->prepare($sql1);
                  $stmt1->bindParam(":user",$user);
                  $stmt1->bindParam(":quiz_id",$quiz_id);
                  $stmt1->execute();
                  ?>



                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $sec;?>">
                          <?php echo $r['sec_id'];?>
                        </a>
                      </h4>
                    </div>
                    <div id="<?php echo $sec;?>" class="panel-collapse collapse">
                      <div class="panel-body">
                        <?php
                        while($r1=$stmt1->fetch())
                        {
                          //echo $r['sec_id'];
                          $ques_id=$r1['ques_id'];
                          $sql2="select * from questions where id='$ques_id'";
                          $stmt2=$dbh->prepare($sql2);
                          $stmt2->execute();
                          $r2=$stmt2->fetch();
                          $eval=$quiz_id."_".$r1['sec_id']."_".$r1['ques_id']."_";
                          ?>

                          <ul class="list-group" >
                            <li class="list-group-item"><?php echo $r2['question_text']."<br>";?></li>
                            <li class="list-group-item"><input value="<?php echo $eval.'1';?>" class="rad_<?php echo $i;?>"type="radio" name="<?php echo $i;?>"><?php echo $r2['option_1']."<br>";?></li>
                            <li class="list-group-item"><input value="<?php echo $eval.'2';?>" type="radio" class="rad_<?php echo $i;?>"name="<?php echo $i;?>"><?php echo $r2['option_2']."<br>";?></li>
                            <li class="list-group-item"><input value="<?php echo $eval.'3';?>" type="radio" class="rad_<?php echo $i;?>"name="<?php echo $i;?>"><?php echo $r2['option_3']."<br>";?></li>
                            <li class="list-group-item"><input value="<?php echo $eval.'4';?>" type="radio" class="rad_<?php echo $i;?>"name="<?php echo $i;?>"><?php echo $r2['option_4']."<br>";?></li>
                            <button type="button" id="but_<?php echo $i;?>" onClick="reset1(this.id)">Reset</button>
                          </ul>   

                          <?php
                          $i++;
                        }?>
                      </div>
                    </div>
                  </div>
                  <?php
                }

                $_SESSION['total_radio']=$i-1;
                ?>
                
              </div>

              <button  type="submit">Submit</button>

            </form>


          </div>
        </div>
      </div>



    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
    <script type="text/javascript">
      function reset1(id)
      {

        $('.rad_'+id).prop('checked',false);
      }
    </script>
    <script>
var ct = setInterval("calculate_time()",100); // Start clock.
function calculate_time()
{

 var end_time = "<?php echo $_SESSION["start_time"]; ?>"; // Get end time from session variable (total time in seconds).
 var dt = new Date(); // Create date object.
 var time_stamp = dt.getTime()/1000; // Get current minutes (converted to seconds).
 var total_time = end_time - Math.round(time_stamp); // Subtract current seconds from total seconds to get seconds remaining.
 var mins = Math.floor(total_time / 60); // Extract minutes from seconds remaining.
 var secs = total_time - (mins * 60); // Extract remainder seconds if any.
 if(secs < 10){secs = "0" + secs;} // Check if seconds are less than 10 and add a 0 in front.
 document.getElementById("txt").value = mins + ":" + secs; // Display remaining minutes and seconds.
 // Check for end of time, stop clock and display message.
 if(mins <= 0)
 {
  if(secs <= 0 || mins < 0)
  {
   clearInterval(ct);
   document.getElementById("txt").value = "0:00";
   $( "#quiz1" ).submit();
 }
}
}
 </script>
 <script>
  var x=1;
  $(window).blur(function() {
    //do something
    if(x==3)
      $( "#quiz1" ).submit();
    else
      alert("You have "+x+"chances left");

    x=x+1; 
  });
</script>

</body>
</html>
<?php
}
?>