<?php
require_once 'include/session.php';
require_once 'include/database.php';
if(!isset($_POST))
	header("Location:logout.php");
$section_name=$_POST['section_name'];
$section_desc=$_POST['section_desc'];
$sec_mark=$_POST['sec_mark'];
$neg_mark=$_POST['neg_mark'];
$quiz_id=$_POST['quiz_id'];
$quiz_creator="x";
$sql="insert into sections(section_name,section_desc,sec_mark,neg_mark,quiz_id) values(:section_name,:section_desc,:sec_mark,:neg_mark,:quiz_id);";
$stmt=$dbh->prepare($sql);
$stmt->bindParam(":section_name",$section_name);
$stmt->bindParam(":section_desc",$section_desc);
$stmt->bindParam(":sec_mark",$sec_mark);
$stmt->bindParam(":neg_mark",$neg_mark);
$stmt->bindParam(":quiz_id",$quiz_id);
if($stmt->execute())
	echo "Done";
else
	echo "Not done";
?>