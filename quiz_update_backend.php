<?php
require_once 'include/session.php';
require_once 'include/database.php';
if(!isset($_POST))
	header("Location:logout.php");
$quiz_name=$_POST['quiz_name'];
$quiz_desc=$_POST['quiz_desc'];
$quiz_id=$_POST['quiz_id'];
$quiz_creator="x";
$sql="update quiz set quiz_name=:quiz_name, quiz_desc=:quiz_desc where quiz_creator=:quiz_creator and id=:quiz_id;";
$stmt=$dbh->prepare($sql);
$stmt->bindParam(":quiz_name",$quiz_name);
$stmt->bindParam(":quiz_desc",$quiz_desc);
$stmt->bindParam(":quiz_creator",$quiz_creator);
$stmt->bindParam(":quiz_id",$quiz_id);
if($stmt->execute())
	echo "Done";
else
	echo "Not done";
?>